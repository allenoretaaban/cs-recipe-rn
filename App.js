import React from "react";
import Setup from "./src/boot/setup";
import 'react-native-gesture-handler';

export default class App extends React.Component {
  render() {
	return <Setup />;
  }
}

console.disableYellowBox = true;
