import SQLite from "react-native-sqlite-storage";

SQLite.DEBUG(true);
SQLite.enablePromise(true);

const database_name = "CsRecipeDB.db";
const database_version = "1.0";
const database_displayname = "SQLite React Offline Database";
const database_size = -1;

export default class Database {

	initDB() {
	  let db;
	  return new Promise((resolve) => {
	    console.log("Plugin integrity check ...");
	    SQLite.echoTest()
	      .then(() => {
	        console.log("Integrity check passed ... Opening database ...");

					var opts = Platform.OS === "ios" ?
						{name:database_name, location:'Documents' } :
            {name:'main', createFromLocation:database_name }

	        SQLite.openDatabase(
						database_name,
						database_version,
						database_displayname,
						database_size
					)
	          .then(DB => {
	            db = DB;

							// console.log('drop all');
							// db.executeSql('DROP TABLE productitems');
							// db.executeSql('DROP TABLE users');

							// db.executeSql('DROP TABLE recipe');
							// db.executeSql('DROP TABLE steps');
							// db.executeSql('DROP TABLE ingredients');

							db.transaction((tx) => {
									tx.executeSql('CREATE TABLE IF NOT EXISTS productitems (' +
										'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
										'recid TEXT NULL, ' +
										'itemNo TEXT NULL, ' +
										'itemname TEXT NULL, ' +
										'dept TEXT NULL, ' +
										'unit TEXT NULL, ' +
										'selling_price TEXT NULL)'
									);
							}).then(() => { /*console.log("Table productitems created successfully");*/ }).catch(error => { /*console.error(error);*/ });

							db.transaction((tx) => {
									tx.executeSql('CREATE TABLE IF NOT EXISTS ingredients (' +
										'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
										'recipeId INTEGER NULL, ' +
										'productItemID TEXT NULL, ' +
										'quantity TEXT NULL, ' +
										'unitselected TEXT NULL, ' +
										'description TEXT NULL, ' +
										'priceperunit TEXT NULL, ' +
										'pricetotal TEXT NULL)'
									);
							}).then(() => { /*console.log("Table ingredients created successfully");*/ }).catch(error => { /*console.error(error);*/ });

							db.transaction((tx) => {
									tx.executeSql('CREATE TABLE IF NOT EXISTS steps (' +
										'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
										'recipeId INTEGER NULL, ' +
										'refQueue INTEGER NULL, ' +
										'content TEXT NULL)'
									);
							}).then(() => { /*console.log("Table steps created successfully");*/ }).catch(error => { /*console.error(error);*/ });

							db.transaction((tx) => {
									tx.executeSql('CREATE TABLE IF NOT EXISTS recipe (' +
										'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
										'title TEXT NULL, ' +
										'description TEXT NULL, ' +
										'servings TEXT NULL, ' +
										'datetimeCreated TEXT NULL, ' +
										'datetimeUpdated TEXT NULL, ' +
										'total TEXT NULL, ' +
										'isOpen INTEGER NULL)'
									);
							}).then(() => { /*console.log("Table recipe created successfully");*/ }).catch(error => { /*console.error(error);*/ });

							db.transaction((tx) => {
									tx.executeSql('CREATE TABLE IF NOT EXISTS users (' +
										'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
										'empId TEXT NULL, ' +
										'refempno TEXT NULL, ' +
										'empNo TEXT NULL, ' +
										'Email TEXT NULL, ' +
										'name TEXT NULL, ' +
										'Branch TEXT NULL, ' +
										'Jobtitle TEXT NULL, ' +
										'pass TEXT NULL, ' +
										'active TEXT NULL, ' +
										'ismobileadmin TEXT NULL)'
									);
							}).then(() => { /*console.log("Table recipe created successfully");*/ }).catch(error => { /*console.error(error);*/ });

							db.transaction((tx) => {
									tx.executeSql('CREATE TABLE IF NOT EXISTS units (' +
										'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
										'recid INTEGER NULL, ' +
										'metric_recid INTEGER NULL, ' +
										'abbv TEXT NULL, ' +
										'description TEXT NULL, ' +
										'deleted TEXT NULL)'
									);
							}).then(() => { /*console.log("Table ingredients created successfully");*/ }).catch(error => { /*console.error(error);*/ });

					    db.close()
	            resolve(db);
	          })
	          .catch(error => {
	            console.error(error);
	          });
	      })
	      .catch(error => {
	        console.log("echoTest failed - plugin not functional");
	      });
	    });
	};

	openDatabase() {
	  return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				resolve(DB);
			})
			.catch(error => {
				console.warn(error);
			});
		});
	};

	addUsers(uiRx) {
	  return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					for(let i = 0; i < uiRx.length; i++) {
						let xRs = [
							null, uiRx[i].empId, uiRx[i].refempno, uiRx[i].empNo, uiRx[i].Email, uiRx[i].name,
							uiRx[i].Branch, uiRx[i].Jobtitle, uiRx[i].pass, uiRx[i].active, uiRx[i].ismobileadmin
						];
						tx.executeSql('INSERT INTO users VALUES (?,?,?,?,?,?,?,?,?,?,?)', xRs).then(([tx, results]) => { console.log(results); });
					}
				}).then((result) => {
					resolve(result);
					this.closeDatabase(DB);
				}).catch((err) => {
					console.warn(err);
					this.closeDatabase(DB);
				});
			});
		});
	};

	addProductItems(piRx) {
	  return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					for(let i = 0; i < piRx.length; i++) {
						let xRs = [null, piRx[i].recid, piRx[i].itemNo, piRx[i].itemname, piRx[i].dept, piRx[i].unit, piRx[i].selling_price+""];
						tx.executeSql('INSERT INTO productitems VALUES (?,?,?,?,?,?,?)', xRs).then(([tx, results]) => { });
					}
				}).then((result) => {
					resolve(result);
					this.closeDatabase(DB);
				}).catch((err) => {
					console.warn(err);
					this.closeDatabase(DB);
				});
			});
		});
	};

	updateRecipe(riRx, iiRx, siRx) {
	  return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					for(let i = 0; i < riRx.length; i++) {
						let rIdx = 0;
						// saving recipe
						tx.executeSql(
							'UPDATE recipe SET title=?, description=?, servings=?, datetimeUpdated=?, total=?' +
							'WHERE id=?',
							riRx[i]
						).then(([tx, results]) => {
							rIdx = riRx[i][5]; // get last saved recipe id

							DB.transaction((tx) => {
								tx.executeSql('DELETE FROM ingredients WHERE recipeId=?', [rIdx]);
								tx.executeSql('DELETE FROM steps WHERE recipeId=?', [rIdx]);
							}).then((result) => {}).catch((err) => {});

							setTimeout(function() {
								DB.transaction((tx) => {
									// // saving ingredients
									for(let j = 0; j < iiRx.length; j++) {
										let rsIng = [null, rIdx, iiRx[j][0], iiRx[j][1], iiRx[j][2], iiRx[j][3], iiRx[j][4], iiRx[j][5]];
										tx.executeSql(
											'INSERT INTO ingredients VALUES (?,?,?,?,?,?,?,?)', rsIng
										).then(([tx, resI]) => {
											// console.log('ingredient id:'+resI.insertId);
										});
									}
									// // saving steps
									for(let k = 0; k < siRx.length; k++) {
										let rsSteps = [null, rIdx, k+1, siRx[k]];
										console.log(rsSteps);
										tx.executeSql(
											'INSERT INTO steps VALUES (?,?,?,?)', rsSteps
										).then(([tx, resS]) => {
											console.log('steps id:'+resS.insertId);
										});
									}
								}).then((result) => {
									resolve(result);
									// this.closeDatabase(DB);
								}).catch((err) => {
									console.warn(err);
									this.closeDatabase(DB);
								});
							}, 500);

						});
					}
				}).then((result) => {
					resolve(result);
					// this.closeDatabase(DB);
				}).catch((err) => {
					console.warn(err);
					this.closeDatabase(DB);
				});
			});
		});
	};

	addRecipe(riRx, iiRx, siRx) {
	  return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					for(let i = 0; i < riRx.length; i++) {
						let rId = 0;
						// saving recipe
						tx.executeSql(
							'INSERT INTO recipe VALUES (?,?,?,?,?,?,?,?)', riRx[i]
						).then(([tx, results]) => {
							console.log('recipe id: '+results.insertId);
							rId = results.insertId; // get last saved recipe id

							DB.transaction((tx) => {
								// saving ingredients
								for(let j = 0; j < iiRx.length; j++) {
									let rsIng = [null, rId, iiRx[j][0], iiRx[j][1], iiRx[j][2], iiRx[j][3], iiRx[j][4], iiRx[j][5]];
									tx.executeSql(
										'INSERT INTO ingredients VALUES (?,?,?,?,?,?,?,?)', rsIng
									).then(([tx, resI]) => {
										console.log('ingredient id:'+resI.insertId);
									});
								}
								// saving steps
								for(let k = 0; k < siRx.length; k++) {
									let rsSteps = [null, rId, k+1, siRx[k]];
									tx.executeSql(
										'INSERT INTO steps VALUES (?,?,?,?)', rsSteps
									).then(([tx, resS]) => {
										console.log('steps id:'+resS.insertId);
									});
								}
							}).then((result) => {
								resolve(result);
								// this.closeDatabase(DB);
							}).catch((err) => {
								console.warn(err);
								this.closeDatabase(DB);
							});

						});
					}
				}).then((result) => {
					resolve(result);
					// this.closeDatabase(DB);
				}).catch((err) => {
					console.warn(err);
					this.closeDatabase(DB);
				});
			});
		});
	};

	deleteRecipe(id) {
		return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					tx.executeSql("DELETE FROM recipe WHERE id = ?", [id]).then(([tx, results]) => {
						console.log(results);
						resolve(results);
					});
				}).then((result) => {
					// this.closeDatabase(db);
				}).catch((err) => {
					console.log(err);
				});
			}).catch(error => {
				console.warn(error);
				this.closeDatabase(DB);
			});
		});
	}

	closeDatabase(DB) {
	  if (DB) {
	    DB.close()
	      .then(status => { console.log("DB CLOSED"); })
	      .catch(error => { console.error(error); });
	  } else {
	    console.error("DB was not OPENED");
	  }
	};

	listProductItems(val) {
	  return new Promise((resolve) => {
	    const products = [];
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
	        tx.executeSql("SELECT * FROM productitems WHERE itemname LIKE ? LIMIT 50", ['%'+val+'%']).then(([tx, results]) => {
	          var len = results.rows.length;
						if (len > 0) {
		          for (let i = 0; i < len; i++) {
		            /*let row = results.rows.item(i);
								/*console.log(row);
								products.push({
									prodId,
									prodName,
									prodImage
								}); */
								products.push(results.rows.item(i));
		          }
						}
						resolve(products);
	        });
	      }).then((result) => {
					// this.closeDatabase(DB);
	      }).catch((err) => {
					console.warn(err);
					this.closeDatabase(DB);
	      });
			}).catch(error => {
				console.warn(error);
				this.closeDatabase(DB);
			});
	  });
	}

	getRSet(table, filter, orderby, val) {
	  return new Promise((resolve) => {
	    const recipies = [];
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					let sqlStr = "SELECT * FROM " + table + " " + filter + " " + orderby; // console.log(sqlStr);
					tx.executeSql(sqlStr, val).then(([tx, results]) => {
	          var len = results.rows.length;
						console.log(results.rows);
						if (len > 0) {
		          for (let i = 0; i < len; i++) {
								recipies.push(results.rows.item(i));
		          }
						}
						resolve(recipies);
	        });
	      }).then((result) => {
					// this.closeDatabase(DB);
	      }).catch((err) => {
	        console.warn(err);
					this.closeDatabase(DB);
	      });
			}).catch(error => {
				console.warn(error);
				this.closeDatabase(DB);
			});
	  });
	}

  deleteRset(table, filter, val) {
		return new Promise((resolve) => {
			SQLite.openDatabase(
				database_name,
				database_version,
				database_displayname,
				database_size
			).then(DB => {
				DB.transaction((tx) => {
					tx.executeSql("DELETE FROM " + table + " " + filter, val).then(([tx, results]) => {
						console.log(results);
						resolve(results);
					});
				}).then((result) => {
					// this.closeDatabase(db);
				}).catch((err) => {
					console.log(err);
				});
			}).catch(error => {
				console.warn(error);
				this.closeDatabase(DB);
			});
	  });
	}

}
