// @flow

import variable from "./../variables/platform";

export default (variables /*: * */ = variable) => {
  const textTheme = {
    ".info": {
      color: variables.brandInfo
    },
    ".success": {
      color: variables.brandSuccess
    },
    ".primary": {
      color: variables.brandPrimary
    },
    textAlignVertical: 'center',
    height: null,
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    ".note": {
      color: "#a7a7a7",
      fontSize: variables.noteFontSize
    }
  };

  return textTheme;
};
