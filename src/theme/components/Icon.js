// @flow

import variable from "./../variables/platform";

export default (variables /*: * */ = variable) => {
  const iconTheme = {
    fontSize: variables.iconFontSize,
    ".info": {
      color: variable.brandInfo
    },
    ".primary": {
      color: variable.brandPrimary
    },
    ".success": {
      color: variable.brandSuccess
    }
  };

  return iconTheme;
};
