const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
import vars from "./../../theme/variables/platform";

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    position: "absolute",
    flex: 1,
    marginTop: deviceHeight / 5,
    marginBottom: 0
  },
  logoTitle: {
    fontSize:57,
    alignSelf:'flex-start',
    marginLeft:60,
    color:vars.brandPrimary,
    height:null
  },
  logoSlogan: {
    fontSize:20,
    alignSelf:'flex-start',
    marginLeft:120,
    color:vars.brandWarning,
    paddingVertical:5,
    height:null
  },
  logo: {
    left: Platform.OS === "android" ? 100 : 100,
    width: 280,
    height: 100
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  },
  overlay: {
    flex: 1,
    backgroundColor:'rgba(119,201,212, 0.4)'
  }
};
