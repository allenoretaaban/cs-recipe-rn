import React, { Component } from "react";
import { ImageBackground, StatusBar, Dimensions, Keyboard, AsyncStorage, KeyboardAvoidingView, SafeAreaView } from "react-native";
import { Container, Button, H3, Text, View, Input, Item, Spinner } from "native-base";
import { Grid, Col, Row } from "react-native-easy-grid";
import Toast, { DURATION } from "react-native-easy-toast";
import Modal from "react-native-modal";
import SplashScreen from 'react-native-splash-screen';

import gstyles from "./styles";
import styles from "./styles";
import vars from "./../../theme/variables/platform";
import cons from "./../../constants/Variables";
import rest from "./../../utility/Requests";

const launchscreenBg = require("../../../assets/swiper-g.png");
const launchscreenLogo = require("../../../assets/logosmall.png");

import Database from "./../../db/Database";
const db = new Database();

class Login extends Component {
  constructor(props) {
    console.log('create constructor');
    super(props);
    this.inputsx = {};
    this.state = {
      isLoading:false, tUsername:"", tPassword:""
    }
  }

  componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      console.log('login componentDidMount');
      SplashScreen.hide();

      db.getRSet(cons.TBL.USER, "", "", null).then((data) => {
        if (data.length > 0) {
          // console.log(data);
          console.log('\n\nHave users!\n');
        } else {
          this.refs.wToast.show('Updating USER RECORDS, please wait....', 1000);
          this.requestUsers();
          setTimeout(() => {
            this.setState({ isLoading:true });
          }, 300);
        }
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading: false });
      })
    });

    this._removeData();
  }

  requestUsers = async() => {
    fetch(rest.SERVER + 'api/getCoPreReq?cn=' + rest.CN + '&branchid=1817114323', { headers:rest.HEADERS })
    .then((response) => response.json()).then((responseJson) => {
      if (responseJson["staff"].length > 0) {
        let rs = responseJson["staff"];
        // console.log(rs[0]);
        db.deleteRset(cons.TBL.USER, "", null).then((data) => {
          db.addUsers(rs).then((data) => {
            // console.log(data);
            this.refs.sToast.show('USER RECORDS successfully UPDATED!!!');
            this.setState({ isLoading:false });
          }).catch((err) => {
            this.refs.eToast.show('Error! Fetched 0 records on USER RECORDS...', DURATION.LONG);
            this.setState({ isLoading:false });
          });
        });
      } else {
        this.refs.eToast.show('Error! Fetched 0 records on USER RECORDS...', DURATION.LONG);
        this.setState({ isLoading:false });
      }
      Keyboard.dismiss();
    })
    .catch((error) => {
      this.setState({ isLoading:false });
      alert("Error on Request! Please inform DEVELOPER....");
      console.warn(error);
    });
  }

  _removeData = async () => {
    try {
      await AsyncStorage.removeItem('csrEmpId');
      return true;
    } catch(error) {
        return false;
    }
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('csrEmpId');
      if (value !== null) {
        console.log('retrieve data');
        this.props.navigation.navigate('Recipe');
        // alert(value);
      }
    } catch (error) {
      console.log(error);
    }
  }

  _storeData = async (data) => {
    try {
      console.log(data);
      await AsyncStorage.setItem('csrEmpId', data.empId+"");
      await AsyncStorage.setItem('csrEmpNo', data.refempno+"");
      await AsyncStorage.setItem('csrName', data.name+"");
    } catch (error) {
      // Error saving data
    }
  }

  doLogin = () => {
    this.setState({ isLoading: true });
    setTimeout(() => {
      db.getRSet(cons.TBL.USER, "WHERE empNo = ? and pass = ?", "", [this.state.tUsername.trim(),this.state.tPassword.trim()])
      .then((data) => {
        this.setState({ isLoading: false });
        // console.log(data[0].name);
        if (data.length > 0) {
          try {
            this._storeData(data[0]);
            this.refs.sToast.show('Welcome! Good day ' +  data[0].name + ' !!!', DURATION.LONG);
            setTimeout(() => {
              this.setState({ isLoading: true });
              setTimeout(() => {
                this.props.navigation.navigate('Recipe');
                this.setState({ isLoading: false });
              }, 2000);
            }, 500);

          } catch (error) {
            // Error saving data
          }
        } else {
          this.refs.eToast.show('INVALID CREDENTIALS! Please try again...', DURATION.LONG);
        }
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading: false });
        this.refs.eToast.show('INVALID CREDENTIALS! Please try again...', DURATION.LONG);
      })
    }, 500);
  }

  render() {
    return (
      <SafeAreaView style={{ flex:1 }}>
        <KeyboardAvoidingView style={{ flex:1 }} enabled={true} behavior={Platform.OS==='ios'?'padding':null} >
          <ImageBackground source={cons.BACKGROUND} style={cons.MAINBG_STYLE}>
            <View style={cons.MAINBG_OVERLAY}>
              <View style={{ top:0, flex:1,  height:cons.DEVICE_HEIGHT }} >
                <View style={{ width:cons.DEVICE_WIDTH, position:'absolute', top:50 }}>
                  <Text style={styles.logoTitle} >CS Recipe</Text>
                  <Text style={styles.logoSlogan} >Discover. Create. Cook.</Text>
                </View>
                <View style={{ alignItems:'center', width:cons.DEVICE_WIDTH, bottom:30, position:'absolute' }}>
                  <Item regular style={{
                      marginLeft:50, marginRight:50, paddingHorizontal:7, marginBottom:5,
                    }} >
                    <Input placeholder='Username' returnKeyType={'next'}
                      blurOnSubmit = {false}
                      value={this.state.tUsername}
                      onChangeText={(value) => { this.setState({ tUsername:value }) }}
                      onSubmitEditing={() => { this.inputsx['tPassword']._root.focus() }}
                    />
                  </Item>
                  <Item regular style={{
                      marginLeft:50, marginRight:50, paddingHorizontal:7, marginBottom:10
                    }} >
                    <Input secureTextEntry={true} placeholder='Password' returnKeyType={'done'}
                      value={this.state.tPassword}
                      onChangeText={(value) => { this.setState({ tPassword:value }) }}
                      ref={(input) => { this.inputsx['tPassword'] = input; }}
                     />
                  </Item>
                  <Button block style={{ marginHorizontal:50, borderColor:'transparent' }}
                    onPress={() => { this.doLogin(); }} >
                    <Text style={{ height:null }}>Log-In</Text>
                  </Button>
                </View>
              </View>
            </View>
          </ImageBackground>
          <Toast ref="eToast" style={{backgroundColor:vars.brandDanger,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
          <Toast ref="iToast" style={{backgroundColor:vars.brandInfo,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
          <Toast ref="sToast" style={{backgroundColor:vars.brandPrimary,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
          <Toast ref="wToast" style={{backgroundColor:vars.brandWarning,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
          <View>
            <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isLoading}>
              <View style={{ flex: 1 }}>
                <Spinner color='#ffffff' style={{ flex:1 }} />
              </View>
            </Modal>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

export default Login;
