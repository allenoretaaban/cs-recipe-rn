const React = require("react-native");
const { Dimensions, Platform } = React;
const pf = Platform.OS;
const deviceHeight = Dimensions.get("window").height;
const rHgt = pf === "ios" ? 50 : 48;

export default {
  container: { backgroundColor:"#fff" },
  titleText: { fontSize:pf==="ios"?25:23,  minHeight:pf==="ios"?22:20, padding:0, margin:0, marginTop:0, paddingTop:0 },
  subTitleText: { height:null, padding:0 },
  yieldingText: { height:null, padding:0, marginTop:7, marginBottom:7, color:'gray' },

  tblHeaderCount: { height:rHgt, borderColor:'#fff', borderTopWidth:1, borderRightWidth:1, width:110,
    backgroundColor:'#77c9d4ff', borderRadius:3, alignItems:'center', justifyContent:'center' },
  tblHeaderUnit: { height:rHgt, borderColor:'#fff', borderTopWidth:1, borderRightWidth:1, width:130,
    backgroundColor:'#77c9d4ff', borderRadius:3, alignItems:'center', justifyContent:'center' },
  tblHeaderDesc: { height:rHgt, borderColor:'#fff', borderTopWidth:1, borderRightWidth:1, flex:1,
    backgroundColor:'#77c9d4ff', borderRadius:3, alignItems:'center', justifyContent:'center' },
  tblHeaderPP: { height:rHgt, borderColor:'#fff', borderTopWidth:1, borderRightWidth:1, width:110, backgroundColor:'#77c9d4ff', borderRadius:3 },
  tblHeaderST: { height:rHgt, borderColor:'#fff', borderTopWidth:1, backgroundColor:'#77c9d4ff', borderRadius:3, width:110 },
  tblHeader: { height:rHgt, borderColor:'#fff', borderTopWidth:1, backgroundColor:'#77c9d4ff', borderRadius:3 },
  tblHeaderPre: { height:rHgt, borderColor:'#fff', borderTopWidth:1, borderRightWidth:1,
      backgroundColor:'#77c9d4ff', borderRadius:3, alignItems:'center', justifyContent:'center' },
  tblHeaderPost: { height:rHgt, borderColor:'#fff', borderTopWidth:1,
      backgroundColor:'#77c9d4ff', borderRadius:3, alignItems:'center', justifyContent:'center' },

  txtHeader: { alignSelf:'center', color:'#fff', textAlignVertical:'center', height:null },
  tblCellCount: { paddingLeft:0, paddingTop:1, borderColor:'#fff', marginLeft:0, borderRightWidth:1, alignItems:'center' },
  tblCellUnit: { paddingLeft:0, paddingTop:1, borderColor:'#fff', borderRightWidth:1, paddingRight:0 },
  tblCellDesc: { paddingLeft:0, paddingTop:1, borderColor:'#fff', borderRightWidth:1 },
  tblCellPP: { paddingLeft:0, paddingTop:1, borderColor:'#fff', borderRightWidth:1 },
  tblCellST: { borderColor:'gray', paddingTop:1, paddingLeft:0 },
  tblCellPre: { height:rHgt, paddingLeft:0, paddingTop:1, borderColor:'#fff', borderRightWidth:1 },
  tblCellPost: { height:rHgt, paddingLeft:0, paddingTop:1, borderColor:'#fff' },
  tblCell: { borderColor:'gray', paddingTop:1, paddingLeft:0 },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  toastcontainer: {
    marginHorizontal: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  }
};
