import vars from "./../../theme/variables/platform";

const React = require("react-native");
const { Dimensions, Platform } = React;
const pf = Platform.OS;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  toastcontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  overlay: {
    flex: 1,
    backgroundColor:'rgba(119,201,212, 0.4)'
  },
  recipe_box: { flexDirection:'row', minHeight:pf==="ios"?46:44, flex:1 },
  recipe_title: { fontSize:23, flex:1, textAlignVertical:'center', height:null, paddingTop:pf==="ios"?9:7, paddingBottom:10 },
  recipe_title_i: { textAlignVertical:'top', color:vars.brandWarning, marginTop:pf==="ios"?11:11 },
  label_gray: { fontSize:pf==="ios"?16:14, marginRight:20, fontFamily:'Lato-Bold', minHeight:18, color:'gray', height:null, marginTop:5 },
  text_gray: { height:null, fontSize:pf==="ios"?16:14, fontWeight:pf==="ios"?'400':'200', color:'gray' },
  label_desc: { marginRight:20, fontFamily:'Lato-Bold', minHeight:20, height:null },
  label_desc_t: { fontWeight:'200', height:null, fontWeight:pf==="ios"?'400':'200', }
};
