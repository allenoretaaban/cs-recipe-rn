import React, { Component } from "react";
import {
  Image, Dimensions, SafeAreaView, FlatList, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback,
  AsyncStorage, ToastAndroid, TouchableOpacity, TouchableHighlight, TouchableNativeFeedback, Alert,
  ImageBackground, ScrollView, Platform, StyleSheet
} from "react-native";
import {
  Container, Header, Title, Content, Text, Button, Icon, Footer, FooterTab, Left, Right, Center, Body, Card,
  CardItem, SwipeRow, Spinner, Fab, Thumbnail, View, Input, Item, Textarea, Picker, TextInput,
} from "native-base";
import { Grid, Col, Row } from "react-native-easy-grid";
import Modal from "react-native-modal";
import Toast, {DURATION} from "react-native-easy-toast";
import { MenuProvider, Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import RNPickerSelect from 'react-native-picker-select';

import helper from "./../../utility/Helpers";
import productItems from "./../../models/ProductItems";
import rest from "./../../utility/Requests";
import vars from "./../../theme/variables/platform";
import cons from "./../../constants/Variables";
import styles from "./styles.form";
import Database from "./../../db/Database";

const db = new Database();
const platform = Platform.OS;

class Update extends Component {

  constructor(props) {
    console.log('update constructor');
    super(props);
    this.inputsx = {}, this.quantityInputsx = {};
    this.recipeItems = [], this.unitItems = [], this.stepItems = [];
    this.unitOptions = cons.UNIT_OPTIONS_C;
    this.state = {
      rsUpdateId:"",
      rsData:[], rQuantity:[], rUnit:[], rDescription:[], rDescriptionId:[],
      rPricePerUnit:[], rPriceTotal: [], rSteps:[],
      rTitle:"", rStory:"", rServings:"", rIcon:[], rTotal:"",
      isModalVisible:false, isLoading:false, itemsData:[],
      showToast: false, itemName:'', typing:false,  gSelectedId:0,
      recipeItemsCount:0, stepItemsCount:1, typingTimeout:0
    }
  }

  componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      console.log('update componentDidMount');

      // re-init
      this.setState({
        rsData:[], rQuantity:[], rUnit:[], rDescription:[], rDescriptionId:[], rPricePerUnit:[], rPriceTotal:[], rSteps:[],
        rTitle:"", rStory:"", rServings:"", rIcon:[], rTotal:"",
        isModalVisible:false, isLoading:false, itemsData:[],
        showToast: false, itemName:'', typing:false,  gSelectedId:0,
        recipeItemsCount:0, stepItemsCount:1, typingTimeout:0,
      });
      this.changeItemName = this.changeItemName.bind(this);
      Keyboard.dismiss();

      this.setState({ isLoading:true });
      setTimeout(() => {
        this._asyncLoadData();
      }, 0);

    });
  }

  _asyncLoadData = async() => {
    try {
      const valx = await AsyncStorage.getItem('recipeId');
      this.setState({ rsUpdateId:valx });
    } catch (error) {
      // Error saving data
    } finally {
      db.getRSet(cons.TBL.RECIPE, "WHERE id = ?", "", [this.state.rsUpdateId])
      .then((data) => {
        if(data.length > 0) {
          let rSet = data[0];
          this.setState({
            rTitle:rSet.title, rStory:rSet.description, rServings:rSet.servings, rsUpdateId:this.state.rsUpdateId
          });
          db.getRSet(cons.TBL.ING, "WHERE recipeId = ?", "", [this.state.rsUpdateId]).then((dti) => {
            let refArr = [], refArrSel = [], refArrIc = [],
              refArrQty = [], refArrUnit = [], refArrDescription = [], refArrDescriptionId = [];
            if (dti.length > 0) {
              for(let j = 0; j < dti.length; j++) {
                refArrQty.push(dti[j].quantity);
                refArrDescription.push(dti[j].description);
                refArrDescriptionId.push(dti[j].productItemID);
                refArrUnit.push(dti[j].unitselected);
                refArrIc.push("refresh");
              }
            }
            this.setState({
              rQuantity:refArrQty, rUnit:refArrUnit, rDescription:refArrDescription, rDescriptionId:refArrDescriptionId,
              rIcon:refArrIc, recipeItemsCount:dti.length
            });
            db.getRSet(cons.TBL.STEP, "WHERE recipeId = ?", "", [this.state.rsUpdateId]).then((dts) => {
              let refArrStep = [];
              if (dts.length > 0) {
                for(let k = 0; k < dts.length; k++) {
                  refArrStep.push(dts[k].content);
                }
                this.setState({
                  stepItemsCount:dts.length, rSteps:refArrStep, isLoading:false
                });
              } else {
                refArrStep.push("");
                this.setState({
                  stepItemsCount:1, rSteps:refArrStep, isLoading:false
                });
              }
            }).catch((err) => {
              console.log(err);
              this.setState({ isLoading:false });
            })
          }).catch((err) => {
            console.log(err);
            this.setState({ isLoading:false });
          })
        }
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading:false });
      })
    }
  }

  handleInputData(value,i) {
    let newArr = [...this.state.rQuantity];
    newArr[i] = value;
    this.setState({ rQuantity:newArr });
  }

  handlePickerSelection(value,i) {
    let newArr = [...this.state.rUnit];
    newArr[i] = value;
    this.setState({ rUnit:newArr });
  }

  handleStepsInputData(value,i) {
    let newArr = [...this.state.rSteps];
    newArr[i] = value;
    this.setState({ rSteps:newArr });
  }

  updateRecipe = () => {
    Keyboard.dismiss();
    this.setState({ isLoading:true });

    setTimeout(() => {
      if(this.state.rTitle.trim() == "") { this.refs.eToast.show('Saving failed! Please input RECIPE TITLE...', 2000); this.setState({ isLoading:false }); return; }
      if(this.state.rServings.trim() == "") { this.refs.eToast.show('Saving failed! Please input YIELD INFO...', 2000); this.setState({ isLoading:false }); return; }
      // validate ingredients
      let rxData = [], ixData = [], sxData = this.state.rSteps, errFlag = false, errEmpty = true;
      for (let i = 0; i < this.recipeItems.length; i++) {
        if (this.state.rQuantity[i].trim() == "" && this.state.rUnit[i].trim() == "Select..." && this.state.rDescription[i].trim() == "") {
        } else {
          errEmpty = false;
          if (this.state.rQuantity[i].trim() == "") { errFlag = true; }
          if (this.state.rUnit[i].trim() == "Select...") { errFlag = true; }
          if (this.state.rDescription[i].trim() == "") { errFlag = true; }
          if (errFlag == false) {
            ixData.push([
              this.state.rDescriptionId[i],
              this.state.rQuantity[i],
              this.state.rUnit[i],
              this.state.rDescription[i],
              this.state.rPricePerUnit[i],
              this.state.rPriceTotal[i]
            ]);
          }
        }
      }
      if(errFlag) {
        this.refs.eToast.show('Saving failed! Please complete the INGREDIENT\'s info...', 2000); this.setState({ isLoading:false }); return;
      }
      if(errEmpty) {
        this.refs.eToast.show('Saving failed! Please add an INGREDIENT...', 2000);  this.setState({ isLoading:false }); return;
      }
      // init recipe data
      rxData.push([this.state.rTitle.trim(), this.state.rStory.trim(), this.state.rServings.trim(), helper.getCurrentDateTime(), this.state.rTotal, parseInt(this.state.rsUpdateId)]);
      // save
      console.log(sxData);
      Alert.alert(
        'Update Recipe', 'Are you sure you want to update this recipe?',
        [
          // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
          { text:'Cancel', onPress:() => { this.setState({ isLoading:false }); }, style:'cancel' },
          { text:'OK', onPress:() => {
              db.updateRecipe(rxData, ixData, sxData).then((data) => {
                this.setState({ isLoading:false });
                // this.refs.sToast.show('Success! A recipe is completely updated...');
                this.props.navigation.navigate('Recipe', { isUpdate:"yes" });
              }).catch((err) => {
                this.refs.eToast.show('Saving failed! Error on saving...', 2000);  this.setState({ isLoading:false });
              })
            }
          },
        ],
        {cancelable: false},
      );
    }, 500);
  }

  searchItem = () => {
    if (this.state.itemName != '') {
      db.listProductItems(this.state.itemName).then((data) => {
        if (data.length > 0) {
          this.setState({ itemsData:data });
        }
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading: false });
      })
    } else {
    }
  }

  requestProductItems = async(val, type) => {
    let thisx = this;
    this.setState({ isLoading:true });
    this.refs.wToast.show('Updating PRODUCT ITEMS, please wait....', 500);
    setTimeout(() => {
      fetch(rest.SERVER + 'api/getcoItemlist?cn=' + rest.CN + '&customerid=-1&itemname='+val, { headers:rest.HEADERS })
      .then((response) => response.json()).then((responseJson) => {
        if (responseJson.length > 0) {
          db.deleteRset("productitems", "", null).then((data) => {
            db.addProductItems(responseJson).then((data) => {
              console.log(data);
              this.refs.sToast.show('PRODUCT ITEMS successfully UPDATED!!!');
              this.setState({ isLoading:false });
            }).catch((err) => {
              this.refs.eToast.show('Error! Fetched 0 records on PRODUCT ITEMS...', DURATION.LONG);
              this.setState({ isLoading:false });
            });
          });
        } else {
          this.refs.eToast.show('Error! Fetched 0 records on PRODUCT ITEMS...', DURATION.LONG);
          this.setState({ isLoading:false });
        }
        Keyboard.dismiss();
      })
      .catch((error) => {
        thisx.setState({ isLoading:false });
        if (error.message.trim() == "Network request failed") {
          thisx.refs.eToast.show('Error on Request! Please check INTERNET CONNECTION....', DURATION.LONG);
        } else {
          thisx.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
        }
      });
    }, 1000);
  }

  keypressHandler = (event) => {
    if (event.key === "Enter") {
      this.searchItem();
    }
  }

  searchItem = async() => {
    if (this.state.itemName != '') {
      // db.listProductItems(this.state.itemName).then((data) => {
      db.getRSet(cons.TBL.ITEM, "WHERE itemname LIKE ?", "GROUP BY itemname", ["%"+this.state.itemName+"%"], "itemname").then((data) => {
        if (data.length == 0) { data.push(cons.PRODITEM_EMPTY); }
        // this.setState({ itemsData:data });
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading:false });
      })
    } else {
    }
  }

  changeItemName = async(obj) => {
    const self = this, dtx = [];
    if (self.state.typingTimeout) {
      dtx.push(cons.PRODITEM_SEARCHING);
      self.setState({ itemsData:dtx });
      clearTimeout(self.state.typingTimeout);
    }
    self.setState({
      itemName: obj.nativeEvent.text,
      typing: false,
      typingTimeout: setTimeout(function () {
        if (self.state.itemName != '') {
          db.getRSet(cons.TBL.ITEM, "WHERE itemname LIKE ?", "GROUP BY itemname", ["%"+self.state.itemName+"%"]).then((data) => {
            if (data.length == 0) { data.push(cons.PRODITEM_EMPTY); }
            self.setState({ itemsData:data });
            console.log(data);
          }).catch((err) => {
            console.log(err);
            this.setState({ isLoading: false });
          })
        }
      }, 1000)
    });
  }

  getReferenceId = (idx) => {
    this.setState({gSelectedId:idx});
    this.toggleModal();
    /*if (this.state.rIcon[idx] == "add") {
      this.toggleModal();
    } else {
      this.refs.eToast.show('Error on DELETE!', DURATION.SHORT);
    }*/
  }

  toggleModal = () => {
    if (!this.state.isModalVisible) {
      this.setState({ itemsData:[] });
    }
    this.setState({ isModalVisible:!this.state.isModalVisible });
  };

  selectIngredient(idx) {
    this.setState({ isModalVisible:false });
    // alert(this.state.itemsData[idx].itemname);
    // console.log(this.state.itemsData[idx].itemname+' => '+this.state.gSelectedId);
    let refDesc = [...this.state.rDescription], refDescId = [...this.state.rDescriptionId], refIconId = [...this.state.rIcon];
    refDesc[this.state.gSelectedId] = this.state.itemsData[idx].itemname;
    this.state.rDescription = refDesc;
    refDescId[this.state.gSelectedId] = this.state.itemsData[idx].recid;
    this.state.rDescriptionId = refDescId;
    refIconId[this.state.gSelectedId] = "refresh";
    this.state.rIcon = refIconId;
  }

  pushStepsItem = (isUpdate) => {
    var i = this.stepItems.length;
    var objDel = this.state.stepItemsCount > 1 ? <TouchableOpacity onPress={() => { this.removeSteps(i); }} style={{ height:null, justifyContent:'center', marginHorizontal:5 }} >
      <Icon name='trash' info style={{ textAlignVertical:'center', marginHorizontal:7 }} />
      </TouchableOpacity> : <Text></Text>;

    this.stepItems.push(
      <Row>
        <View style={{ flex:1 }}>
          <Card regular style={{ minHeight:platform==="ios"?54:50, paddingHorizontal:7, marginRight:7, marginLeft:7, marginTop:7,
            borderRadius:3, borderColor:'#77c9d4ff',  flex:1, padding:0 }}>
            <View style={{ flex:1, flexDirection:'row' }}>
              <Icon name='nutrition' info style={{ marginHorizontal:7, marginTop:12 }} />
              <Text info style={{ height:null, textAlignVertical:'top', paddingTop:13 }}>Step {i+1}:</Text>
              <Input multiline placeholder='Put your instructions here...' style={{ marginHorizontal:5, height:null, marginTop:platform==="ios"?9:13, marginBottom:platform==="ios"?12:10, textAlignVertical:'top', padding:0  }}
                key={i}
                value={this.state.rSteps[i]}
                ref={this.state.rSteps[i]}
                onChangeText={(value) => this.handleStepsInputData(value, i)}
                blurOnSubmit = {true}
              />
              {objDel}
            </View>
          </Card>
        </View>
      </Row>
    )
    if(isUpdate) {
      this.forceUpdate();
      this.state.stepItemsCount++;
    }
  }

  removeSteps = (refId) => {
    if (this.state.stepItemsCount > 1) {
      this.stepItems.splice(refId,1);

      let refArr = this.state.rSteps;
      refArr.splice(refId,1);
      this.setState({ rSteps:refArr });

      this.state.stepItemsCount--;
      this.forceUpdate();
    }
  }

  pushIngredientItem = (isUpdate) => {
    var i = this.recipeItems.length;
    // console.log("xxx"+i);
    this.recipeItems.push(
      <Row>
        <Grid>
          <Col size={1.5} style={styles.tblCellPre}>
            <Item regular info style={{ borderRadius:2, marginLeft:0, height:platform==="ios"?49:47 }}>
              <Input
                key={i}
                value={this.state.rQuantity[i]}
                style={{ paddingVertical:1, textAlign:'right', paddingRight:10, paddingLeft:10, fontSize:platform==="ios"?19:17, fontWeight:'bold' }}
                keyboardType={'numeric'}
                onChangeText={(value) => this.handleInputData(value, i)}
                returnKeyType={'next'}
                onSubmitEditing={() => { if(this.quantityInputsx[i+1] != null){this.quantityInputsx[i+1]._root.focus()}else{Keyboard.dismiss()} }}
                blurOnSubmit = {false}
                ref={(input) => { this.quantityInputsx[i] = input; }}
              />
            </Item>
          </Col>
          <Col size={1.5} style={styles.tblCellPre}>
            <View style={{ borderWidth:1, borderColor:'#77c9d4ff', borderRadius:2, flex:1 }}>
              <RNPickerSelect
                skey={i}
                placeholder={{ label:'Select...', value:null, color:'#015249' }}
                useNativeAndroidPickerStyle={true}
                onValueChange={(value) => this.handlePickerSelection(value, i)}
                value={this.state.rUnit[i]}
                items={this.unitItems}
                style={{ ...cons.PICKERSTYLES, iconContainer: { top:10, right:0 },
                  placeholder: { color:'gray', fontSize:platform==="ios"?18:16, fontWeight:'400', fontFamily:"Lato-Regular" }
                }}
              />
            </View>
          </Col>
          <Col size={7} style={styles.tblCellPre} >
            <View style={{ borderWidth:1, borderColor:'#77c9d4ff', paddingHorizontal:5, borderRadius:2, flex:1, justifyContent:'center', alignItems:'center', height:null }}>
              <View style={{ justifyContent:'center', flex:1, flexDirection:'row', alignItems:'center', height:null }}>
                <Text style={{ flex:1, color:'#015249', fontSize:platform==="ios"?17:15, marginHorizontal:3, textAlignVertical:'center', height:null }}>{this.state.rDescription[i]}</Text>
                <TouchableOpacity style={{ height:null, width:null, paddingHorizontal:10 }} onPress={() => { this.getReferenceId(i) }}>
                  <Icon active name={this.state.rIcon[i]} style={{ color:'#a5a5af', alignSelf:'center', backdropColor:'red', height:null, fontSize:platform==="ios"?26:24 }} />
                </TouchableOpacity>
              </View>
            </View>
          </Col>
          <Col size={1.5} style={styles.tblCellPre} >
            <View style={{ borderWidth:1, borderColor:'#77c9d4ff', paddingHorizontal:5, borderRadius:2, flex:1 }}>
              <Grid>
                <View style={{ justifyContent:'center', flex:1 }}>
                  <Text style={{color:'#015249', alignSelf:'flex-end', marginHorizontal:3, textAlignVertical:'center', height:null }}>{this.state.rPricePerUnit[i]}</Text>
                </View>
              </Grid>
            </View>
          </Col>
          <Col size={1.5} style={styles.tblCellPost} >
            <View style={{ borderWidth:1, borderColor:'#77c9d4ff', paddingHorizontal:5, borderRadius:2, flex:1 }}>
              <Grid>
                <View style={{ justifyContent:'center', flex:1 }}>
                  <Text style={{color:'#015249', alignSelf:'flex-end', marginHorizontal:3, textAlignVertical:'center', height:null }}>{this.state.rPriceTotal[i]}</Text>
                </View>
              </Grid>
            </View>
          </Col>
        </Grid>
      </Row>
    )
    if(isUpdate) {
      this.forceUpdate();
      this.state.recipeItemsCount++;
      this.state.rQuantity[i] = "";
      this.state.rUnit[i] = "Select...";
      this.state.rDescription[i] = "";
      this.state.rDescriptionId[i] = "";
      this.state.rIcon[i] = "add";
    }
  }

  onMenuSelect = (val) => {
    switch(val) {
      case 2:
        Alert.alert( 'Logout', 'Are you sure you want to log-out?',
          [
            { text:'Cancel', onPress:() => { this.setState({ isLoading:false }); }, style:'cancel' },
            { text:'OK', onPress:() => {
              this.setState({ isLoading:true });
              setTimeout(() => {
                this.setState({ isLoading:false });
                this.props.navigation.navigate('Login')
              }, 500);
            }
            },
          ],
          {cancelable: false},
        );
        break;
      case 1:
        Alert.alert( 'Logout', 'Are you sure you want to UPDATE PRODUCT ITEM records?',
          [
            { text:'Cancel', onPress:() => { this.setState({ isLoading:false }); }, style:'cancel' },
            { text:'OK', onPress:() => {
                this.requestProductItems('', 0);
              }
            },
          ],
          {cancelable: false},
        );
        break;
      default:
        Alert.alert( 'About', 'CreativeScript Recipe Mobile App \u00A9 2020',
          [
            { text:'OK', onPress:() => { }, style:'cancel' },
          ],
          {cancelable: false},
        );
        break;
    }
  }

  render() {

    // ingredients items
    this.unitItems = [];
    for (var i = 0; i < this.unitOptions.length; i++) {
      let uval = this.unitOptions[i][2];
      let uvalStr = this.unitOptions[i][3];
      // this.unitItems.push( <Picker.Item label={uval} value={uval} style={{ color:'red' }}/> )
      this.unitItems.push({ label:uval, value:uval });
    }
    this.recipeItems = [];
    for(let i = 0; i < this.state.recipeItemsCount; i++) {
      this.pushIngredientItem(false);
    }

    // steps items
    this.stepItems = [];
    for(let i = 0; i < this.state.stepItemsCount; i++) {
      this.pushStepsItem();
    }

    return (
      <Container style={styles.container}>
        <MenuProvider style={{flexDirection:'column', padding:0 }}>
          <Header
            style={{ backgroundColor: "#57bc90ff", paddingRight:5 }}
            androidStatusBarColor="#015249"
            iosBarStyle="light-content">
            <View style={{ justifyContent:'center' }}>
              <Button transparent onPress={() => this.props.navigation.navigate('Recipe')} >
                <Icon name="arrow-back" style={{ color:'#fff' }}/>
              </Button>
            </View>
            <Body style={{ marginLeft:5, alignItems:'flex-start' }}>
              <Title style={{ color:'#fff' }}>Update Recipe</Title>
            </Body>
            <Right>
              <Menu onSelect={value => { this.onMenuSelect(value); }} style={{ height:null }}>
                <MenuTrigger text={ <Icon style={{ color:'#fff' }} name="more" /> }
                  style={cons.STYLE_MTRIG}/>
                <MenuOptions>
                  <MenuOption value={0} style={cons.STYLE_MOPT}>
                    <Icon name="eye" style={cons.STYLE_MOPT_I}/>
                    <Text success style={cons.STYLE_MOPT_T}>About</Text>
                  </MenuOption>
                  <MenuOption value={1} style={cons.STYLE_MOPT}>
                    <Icon name="archive" style={cons.STYLE_MOPT_I}/>
                    <Text success style={cons.STYLE_MOPT_T}>Update Product Items</Text>
                  </MenuOption>
                    <MenuOption value={2} style={cons.STYLE_MOPT}>
                      <Icon name="lock" style={cons.STYLE_MOPT_I}/>
                      <Text success style={cons.STYLE_MOPT_T}>Log-Out</Text>
                    </MenuOption>
                </MenuOptions>
              </Menu>
            </Right>
          </Header>
          <ImageBackground source={cons.BACKGROUND} style={cons.MAINBG_STYLE}>
            <View style={cons.MAINBG_OVERLAY}>
              <Content padder>
                <Card>
                  <CardItem header bordered style={{ paddingLeft:10, paddingTop:10, paddingBottom:1 }}>
                    <View style={{ marginBottom:5, marginLeft:5, flex:1 }} >
                      <Item style={{ paddingTop:0, marginTop:0, alignItems:'flex-start' }}>
                        <Icon name='paper' info style={{ textAlignVertical:'top', color:vars.brandWarning, marginTop:platform==="ios"?4:3 }} />
                        <Input multiline style={styles.titleText} primary selectTextOnFocus={true} placeholder="Recipe Title" value={this.state.rTitle}
                        onChangeText={(value) => { this.setState({ rTitle:value }) }} placeholderTextColor={vars.brandPrimary} returnKeyType={'next'} blurOnSubmit = {true}
                        onSubmitEditing={() => { this.inputsx['description']._root.focus() }}></Input>
                      </Item>
                      <Item style={{ paddingTop:0, marginTop:0, alignItems:'flex-start' }}>
                        <Icon name='pizza' info style={{ textAlignVertical:'top', color:vars.brandWarning, marginTop:platform==="ios"?5:3 }} />
                        <Input multiline style={styles.subTitleText} info note selectTextOnFocus={true} placeholder="Place your story here..." value={this.state.rStory}
                        onChangeText={(value) => { this.setState({ rStory:value }) }} placeholderTextColor={vars.brandInfo} returnKeyType={'next'} blurOnSubmit = {true}
                        ref={(input) => { this.inputsx['description'] = input; }} onSubmitEditing={() => { this.inputsx['yield']._root.focus() }}></Input>
                      </Item>
                      <Input multiline style={styles.yieldingText} success note selectTextOnFocus={true} placeholder="How many servings (yield) ?" value={this.state.rServings}
                        onChangeText={(value) => { this.setState({ rServings:value }) }} placeholderTextColor={vars.brandSuccess} returnKeyType={'done'} blurOnSubmit = {true}
                        ref={(input) => { this.inputsx['yield'] = input; }}></Input>
                    </View>
                  </CardItem>
                </Card>
                <Card>
                  <CardItem header bordered style={{ paddingLeft:20, paddingTop:3, paddingBottom:3, height:platform==="ios"?46:42 }}>
                    <Text style={{ fontSize:platform==="ios"?19:17, height:null }} success>Ingredients</Text>
                  </CardItem>
                  <CardItem style={{ paddingLeft:3, paddingTop:3, paddingRight:3, paddingBottom:3 }} listItemPadding={0} >
                    <Grid>
                      <Row>
                        <Grid>
                          <Col size={1.5} style={styles.tblHeaderPre}>
                            <Text style={styles.txtHeader}>Quantity</Text>
                          </Col>
                          <Col size={1.5} style={styles.tblHeaderPre}>
                            <Text style={styles.txtHeader}>Unit</Text>
                          </Col>
                          <Col size={7} style={styles.tblHeaderPre}>
                            <Text style={styles.txtHeader}>Description</Text>
                          </Col>
                          <Col size={1.5} style={styles.tblHeaderPre}>
                            <Text style={styles.txtHeader}>Price/Unit</Text>
                          </Col>
                          <Col size={1.5} style={styles.tblHeaderPost}>
                            <Text style={styles.txtHeader}>Price</Text>
                          </Col>
                        </Grid>
                      </Row>
                      {this.recipeItems}
                      <Row>
                        <Grid>
                          <Col size={1} style={styles.tblCellPre} >
                            <View style={{ borderWidth:1, borderColor:'#77c9d4ff', paddingHorizontal:5, borderRadius:2, height:platform==="ios"?50:48 }}>
                              <Grid>
                                <View style={{ justifyContent:'center', flex:1 }}>
                                  <Text style={{color:'#015249', alignSelf:'flex-end', marginHorizontal:10, textAlignVertical:'center', height:null }}>Total : 0.00</Text>
                                </View>
                              </Grid>
                            </View>
                          </Col>
                        </Grid>
                      </Row>
                    </Grid>
                  </CardItem>
                  <CardItem style={{ paddingLeft:15, paddingTop:10, paddingBottom:13 }}>
                    <View style={{flex:1}}>
                      <Button rounded style={styles.mb14} style={{ alignSelf:'flex-end', backgroundColor:'#a5a5af', height:platform==="ios"?46:42 }} onPress={() => {this.pushIngredientItem(true)}}>
                        <Text style={{ height:null }}>+ Ingredient</Text>
                      </Button>
                    </View>
                  </CardItem>
                </Card>
                <Card>
                  <CardItem header bordered style={{ paddingLeft:20, paddingTop:3, paddingBottom:3, height:platform==="ios"?46:42 }}>
                    <Text style={{ fontSize:platform==="ios"?19:17, }} success >Steps</Text>
                  </CardItem>
                  <CardItem style={{ paddingLeft:3, paddingTop:3, paddingRight:3, paddingBottom:3 }} listItemPadding={0} >
                    <Grid>
                    {this.stepItems}
                    </Grid>
                  </CardItem>
                  <CardItem style={{ paddingTop:3, paddingBottom:13 }}>
                    <View style={{flex:1}}>
                      <Button rounded style={{ alignSelf:'flex-end', backgroundColor:'#a5a5af', height:platform==="ios"?46:42 }} onPress={() => {this.pushStepsItem(true)}}>
                        <Text style={{ height:null }}>+ Step</Text>
                      </Button>
                    </View>
                  </CardItem>
                </Card>
                <View style={{ height:80 }}></View>
              </Content>
            </View>
          </ImageBackground>
        </MenuProvider>
        <Toast ref="eToast" style={{backgroundColor:vars.brandDanger,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
        <Toast ref="iToast" style={{backgroundColor:vars.brandInfo,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
        <Toast ref="sToast" style={{backgroundColor:vars.brandPrimary,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
        <Toast ref="wToast" style={{backgroundColor:vars.brandWarning,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
        <View>
          <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isLoading}>
            <View style={{ flex: 1 }}>
              <Spinner color='#ffffff' style={{ flex:1 }} />
            </View>
          </Modal>
        </View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#015249' }}
          position="bottomRight"
          onPress={() => { this.updateRecipe(); }}>
          <Icon name="save" />
        </Fab>
        <View>
          <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isModalVisible}
            hideModalContentWhileAnimating={true} style={{ margin:10 }}
            onRequestClose={() => { this.setState({ isModalVisible:false }) }}>
            <View style={{ flex:1, backgroundColor:'transparent' }}>
              <Card style={{ backgroundColor:'transparent', flex:1 }}>
                <CardItem header style={{ paddingLeft:20, paddingTop:0, paddingBottom:0, paddingRight:5,
                  backgroundColor:"#57bc90ff", height:platform==="ios"?54:50 }}>
                  <Title style={{ flex:1, color:'#fff', textAlign:'left' }}>Search and Select Item</Title>
                  <TouchableOpacity onPress={() => { this.setState({ isModalVisible:false }) }} >
                    <Icon name='close' style={{ textAlignVertical:'center', color:'#fff', fontSize:platform==="ios"?36:24 }} />
                  </TouchableOpacity>
                </CardItem>
                <CardItem style={{ paddingTop:7, paddingBottom:5, paddingLeft:5, paddingRight:5, height:null }}>
                  <Item rounded style={{ backgroundColor:'rgba(119, 201, 212, 0.2)', paddingLeft:10, paddingRight:10, height:platform==="ios"?54:50, flex:1, justifyContent:'center' }}>
                    <Input placeholder='Search here...' placeholderTextColor="#a5a5af" onChange={this.changeItemName} style={{ height:null, marginBottom:platform==="ios"?5:0 }}
                      onChangeText={(text) => this.setState({itemName:text})} returnKeyType='search' />
                    <Icon primary name='search' onPress={this.searchItem} />
                  </Item>
                </CardItem>
                <CardItem style={{ flex:1, paddingLeft:0, paddingTop:0, paddingBottom:0, paddingRight:0, alignItems:'flex-start' }}>
                  <ScrollView>
                    <FlatList
                      style={{marginTop:0}}
                      data={ this.state.itemsData }
                      ItemSeparatorComponent = {this.FlatListItemSeparator}
                      renderItem={({item, index}) => item.itemNo == "empty" || item.itemNo == "searching" ?
                        <View style={{ flex:1, margin:50, alignItems:'center' }}>
                          <Text primary style={{ marginLeft:12, marginRight:12, flex:1, textAlign:'center' }}>{item.itemname}</Text>
                        </View> :
                        <TouchableOpacity onPress={() => {this.selectIngredient(index)}}
                          style={{ borderRadius:3, borderColor:'#77c9d4ff', borderWidth:1, margin:3, height:platform==="ios"?54:50 }}>
                          <View style={{ flex:1, margin:5, flexDirection:'row', alignItems:'center' }}>
                            <Text primary style={{ marginLeft:12, marginRight:12, height:null }}>
                              {item.itemname} {item.selling_price == "0" || item.selling_price == "null" ? "" : " | " + parseFloat(item.selling_price).toFixed(2) }
                            </Text>
                          </View>
                        </TouchableOpacity>
                      }
                      keyExtractor={(item, index) => index.toString()}
                    />
                  </ScrollView>
                </CardItem>
              </Card>
            </View>
          </Modal>
        </View>
      </Container>
    );
  }
}

export default Update;
