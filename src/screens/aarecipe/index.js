import React, { Component } from "react";
import { FlatList, TouchableOpacity, ImageBackground, BackHandler, DeviceEventEmitter, AsyncStorage, Alert,
  ScrollView, Platform
} from "react-native";
import {
  Container, Header, View, Body, Button, Icon, Title, Left, Right, Center, Content, Card, CardItem, Text,
  Item, Input, Spinner, Fab
} from "native-base";
import Toast, {DURATION} from "react-native-easy-toast";
import Modal from "react-native-modal";
import SplashScreen from 'react-native-splash-screen';

import styles from "./styles";
import vars from "./../../theme/variables/platform";
import cons from "./../../constants/Variables";

import Database from "./../../db/Database";
const db = new Database();
const launchscreenBg = require("../../../assets/swiper-g.png");
const platform = Platform.OS;

class Recipe extends Component {

  constructor(props) {
    console.log('recipe constructor');
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      itemsData:[], isLoading:false, isModalVisible:false
    }
  }

  componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      console.log('recipe componentDidMount');
      SplashScreen.hide();

      console.log(this.props.navigation.state.params);
      if(this.props.navigation.state.params) {
        if (this.props.navigation.state.params.isUpdate == "yes") {
          this.refs.sToast.show('Success! A recipe is completely updated...', DURATION.LONG);
          this.props.navigation.setParams({ isUpdate:null });
        }
        if (this.props.navigation.state.params.isCreated == "yes") {
          this.refs.sToast.show('Success! A recipe is completely created...', DURATION.LONG);
          this.props.navigation.setParams({ isCreated:null });
        }
      }

      // this.setState({ isLoading:true });
      this.loadView();
    });
  }

  loadView = async() => {
    try {
      setTimeout(() => {
        db.getRSet(cons.TBL.RECIPE, "", " ORDER BY id DESC", null).then((data) => {
          if (data.length > 0) {
            this.setState({itemsData:data});
          } else {
            this.setState({itemsData:[]});
          }
          this.forceUpdate();
          this.setState({ isLoading:false });
        }).catch((err) => {
          console.log(err);
          this.setState({ isLoading:false });
        })
      }, 100);
    } catch (error) {

    }
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
    return true;
  }

  showDetails = (id) => {
    this.setState({ isModalVisible:!this.state.isModalVisible });

    let rsID = id;
    this.setState({ isLoading: true });
    setTimeout(() => {
      let rsR = [], rsI = [], rsS = [];
      db.getRSet(cons.TBL.RECIPE, "WHERE id = ?", "", [rsID]).then((data) => {
        console.log(data);
        if (data.length > 0) {
          rsR = data[0];

          // display recipies
          db.getRSet(cons.TBL.ING, "WHERE recipeId = ?", "", [rsID]).then((dti) => {
  					for(let j = 0; j < dti.length; j++) {
              rsI.push(
                <Text primary style={{ fontWeight:platform==="ios"?'400':'200' }}>{dti[j].quantity} {dti[j].unitselected}, {dti[j].description}{"\n"}</Text>
              )
            }
            this.setState({ sIngredients:rsI });
          }).catch((err) => {
            console.log(err);
            this.setState({ isLoading: false });
          })

          // display steps
          db.getRSet(cons.TBL.STEP, "WHERE recipeId = ?", "", [rsID]).then((dti) => {
            for(let k = 0; k < dti.length; k++) {
              rsS.push(
                <Text primary style={{ fontWeight:platform==="ios"?'400':'200' }}>{k+1}. {dti[k].content}{"\n"}
                </Text>
              )
            }
            this.setState({ sSteps:rsS });
          }).catch((err) => {
            console.log(err);
            this.setState({ isLoading: false });
          })

          this.setState({
            isLoading:false, sTitle:rsR.title,
            sDesc:rsR.description, sYield:rsR.servings, sRecipeId:rsR.id, sTotal:rsR.total
          });
        } else {
          this.setState({ isLoading: false });
        }
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading: false });
      })
    }, 300);
  }

  updateRecipe = async(val, flag) => {
    if (flag) this.setState({ isModalVisible:!this.state.isModalVisible });
    try {
      await AsyncStorage.setItem('recipeId', val+"");
    } catch (error) {
      console.log(error);
      // Error saving data
    } finally {
      this.props.navigation.navigate('Update', { xid:val });
    }
  }

  deleteRecipe = async(val) => {
    Alert.alert( 'Delete Recipe', 'Are you sure you want to delete this recipe?',
      [
        { text:'Cancel', onPress:() => { this.setState({ isLoading:false }); }, style:'cancel' },
        { text:'OK', onPress:() => {
          try {
            db.deleteRset(cons.TBL.RECIPE, "WHERE id = ?", [val]).then((data) => {
              this.loadView();
            });
          } catch (error) {

          }
        }
        },
      ],
      {cancelable: false},
    );
  }

  render() {
    return (
      <Container>
          <Header
            style={{ backgroundColor: "#57bc90ff" }}
            androidStatusBarColor="#015249"
            iosBarStyle="light-content">
            <View style={{ justifyContent:'center' }}>
              <Button transparent onPress={() => this.props.navigation.openDrawer()} >
                <Icon name="ios-menu" style={{ color:'#fff' }}/>
              </Button>
            </View>
            <Body style={{ marginLeft:5, alignItems:'flex-start' }}>
              <Title style={{ color:'#fff' }}>Recipies</Title>
            </Body>
            <Right>
              <Button block onPress={() => { this.props.navigation.navigate('Create'); }} style={{ padding:0, height:34, alignSelf:'center' }}>
                <Text style={{ height:null, color:'#fff', fontSize:platform==="ios"?17:15 }}>Create</Text>
              </Button>
              <Button transparent>
                <Icon name="search" style={{ color:'#fff', height:null, fontSize:24 }}/>
              </Button>
            </Right>
          </Header>
          <ImageBackground source={cons.BACKGROUND} style={cons.MAINBG_STYLE}>
            <View style={cons.MAINBG_OVERLAY}>
              <Content>
                <View style={{padding:5}}>
                  <FlatList
                  data={ this.state.itemsData }
                  ItemSeparatorComponent = {this.FlatListItemSeparator}
                  renderItem={({item, index}) =>
                    <View style={{ flex:1 }}>
                      <Card regular>
                        <CardItem header bordered style={{ paddingHorizontal:20, flex:1, paddingTop:0, paddingBottom:0 }}>
                          <View style={styles.recipe_box}>
                            <Icon name='paper' info style={styles.recipe_title_i} />
                            <Text style={styles.recipe_title}>  {item.title}</Text>
                          </View>
                        </CardItem>
                        <CardItem style={{ paddingTop:10, paddingBottom:0 }}>
                          <View style={{ flexDirection:'column' }}>
                              <Text success style={styles.label_desc}>Description:
                                <Text primary style={styles.label_desc_t}> {item.description}</Text>
                              </Text>
                              <Text success style={styles.label_gray}>Yield:
                                <Text primary style={styles.text_gray}> {item.servings}</Text>
                              </Text>
                              <Text success style={styles.label_gray}>Price per Yield:
                                <Text primary style={styles.text_gray}> {item.total}</Text>
                              </Text>
                          </View>
                        </CardItem>
                        <CardItem footer style={{ paddingHorizontal:20, paddingVertical:0 }}>
                          <View style={{ flex:1, flexDirection:'row-reverse' }}>
                            <View>
                              <Button rounded warning style={{ marginHorizontal:3, alignSelf:'flex-end', height:platform==="ios"?44:42 }} onPress={() => { this.deleteRecipe(item.id); }}>
                                <Icon name="trash" style={{color:'#fff',marginRight:0}}/>
                                <Text style={{ paddingLeft:5, height:null }}>Delete</Text>
                              </Button>
                            </View>
                            <View>
                              <Button rounded success style={{ marginHorizontal:3, alignSelf:'flex-end', height:platform==="ios"?44:42 }} onPress={() => { this.updateRecipe(item.id, false); }}>
                                <Icon name="redo" style={{color:'#fff',marginRight:0}}/>
                                <Text style={{ paddingLeft:5, height:null }}>Edit</Text>
                              </Button>
                            </View>
                            <View>
                              <Button rounded info style={{ marginHorizontal:3, alignSelf:'flex-end', height:platform==="ios"?44:42 }} onPress={() => {this.showDetails(item.id);}}>
                                <Icon name="document" style={{color:'#fff',marginRight:0}}/>
                                <Text style={{ paddingLeft:5, height:null }}>View</Text>
                              </Button>
                            </View>
                          </View>
                        </CardItem>
                      </Card>
                    </View>
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
                </View>
              </Content>
            </View>
            <View>
              <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isModalVisible}
                hideModalContentWhileAnimating={true} style={{ margin:10 }}
                onRequestClose={() => { this.setState({ isModalVisible:false }) }}>
                <View style={{ flex:1 }}>
                  <Card regular style={{ flex:1 }}>
                    <CardItem header bordered style={{ paddingLeft:20, paddingRight:5, paddingTop:0, paddingBottom:0, minHeight:platform==="ios"?50:44, backdropColor:'red' }}>
                      <View style={styles.recipe_box}>
                        <Icon name='paper' info style={styles.recipe_title_i} />
                        <Text style={styles.recipe_title}>  {this.state.sTitle}</Text>
                      </View>
                      <TouchableOpacity onPress={() => { this.setState({ isModalVisible:false }) }} >
                        <Icon name='close' info style={{ textAlignVertical:'center', fontSize:platform==="ios"?34:22 }} />
                      </TouchableOpacity>
                    </CardItem>
                    <CardItem style={{ paddingTop:10, paddingBottom:0, flex:1, alignItems:'flex-start' }}>
                      <ScrollView style={{ flex:1, height:null }}>
                        <View style={{ flexDirection:'column' }}>
                            <Text success style={styles.label_desc}>Description:
                              <Text primary style={styles.label_desc_t}> {this.state.sDesc}</Text>
                            </Text>
                            <Text success style={styles.label_gray}>Yield:
                              <Text primary style={styles.text_gray}> {this.state.sYield}</Text>
                            </Text>
                            <Text success style={styles.label_gray}>Price per Yield:
                              <Text primary style={styles.text_gray}> {this.state.sTotal}</Text>
                            </Text>
                            <Text info style={{ marginTop:17, marginRight:20, fontFamily:'Lato-Bold', height:null }}>Ingredients:{"\n"}
                              {this.state.sIngredients}
                            </Text>
                            <Text info style={{ marginTop:0, marginRight:20, fontFamily:'Lato-Bold', height:null }}>How to Cook:{"\n"}
                              {this.state.sSteps}
                            </Text>
                        </View>
                      </ScrollView>
                    </CardItem>
                    <CardItem footer style={{ paddingHorizontal:20, paddingVertical:0 }}>
                      <View style={{ flex:1, flexDirection:'row-reverse' }}>
                        <View>
                          <Button rounded warning style={{ marginHorizontal:3, alignSelf:'flex-end', height:platform==="ios"?44:42 }} onPress={() => { this.deleteRecipe(this.state.sRecipeId); }}>
                            <Icon name="trash" style={{color:'#fff',marginRight:0}}/>
                            <Text style={{ paddingLeft:5, height:null }}>Delete</Text>
                          </Button>
                        </View>
                        <View>
                          <Button rounded success style={{ marginHorizontal:3, alignSelf:'flex-end', height:platform==="ios"?44:42 }} onPress={() => { this.updateRecipe(this.state.sRecipeId, true); }}>
                            <Icon name="redo" style={{color:'#fff',marginRight:0}}/>
                            <Text style={{ paddingLeft:5, height:null }}>Edit</Text>
                          </Button>
                        </View>
                      </View>
                    </CardItem>
                  </Card>
                </View>
              </Modal>
              <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isLoading}>
                <View style={{ flex: 1 }}>
                  <Spinner color='#ffffff' style={{ flex:1 }} />
                </View>
              </Modal>
            </View>
            <Toast ref="eToast" style={{backgroundColor:vars.brandDanger,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
            <Toast ref="iToast" style={{backgroundColor:vars.brandInfo,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
            <Toast ref="sToast" style={{backgroundColor:vars.brandPrimary,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
            <Toast ref="wToast" style={{backgroundColor:vars.brandWarning,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='center' fadeOutDuration={1000}/>
          </ImageBackground>
      </Container>
    );
  }
}

export default Recipe;
