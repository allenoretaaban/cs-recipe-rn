const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    position: "absolute",
    flex: 1,
    marginTop: deviceHeight / 5,
    marginBottom: 0
  },
  logoTitle: {
    fontWeight:'bold',
    fontSize:60,
    alignSelf:'center',
    marginTop:150,
    color:'#015249',
    height:null
  },
  logoSlogan: {
    fontSize:27,
    alignSelf:'center',
    marginTop:-10,
    color:'orange'
  },
  logo: {
    left: Platform.OS === "android" ? 100 : 100,
    width: 280,
    height: 100
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  },
  overlay: {
    flex: 1,
    backgroundColor:'rgba(119, 201, 212, 0.5)'
  }
};
