import React, { Component } from "react";
import { ImageBackground, StatusBar, Dimensions } from "react-native";
import { Container, Button, H3, Text, View } from "native-base";
import { Grid, Col, Row } from "react-native-easy-grid";

import styles from "./styles";

const launchscreenBg = require("../../../assets/swiper-g.png");
const launchscreenLogo = require("../../../assets/logosmall.png");
const deviceWidth = Dimensions.get("window").width;

class Home extends Component {
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.overlay}>
            <View style={styles.logoContainer}>
              <Grid>
                <Row>
                  <View style={{ width:deviceWidth, height:null }}><Text style={styles.logoTitle} >CS Recipe</Text></View>
                </Row>
                <Row>
                  <View style={{ width:deviceWidth }}><Text style={styles.logoSlogan} >Discover. Create. Cook.</Text></View>
                </Row>
              </Grid>
            </View>
            <View style={{ marginBottom:80, position:'absolute', bottom:0, alignItems:'center', width:deviceWidth }}>
              <Button style={{ backgroundColor: "#015249", marginHorizontal:50, alignSelf: 'center' }}
                onPress={() => this.props.navigation.openDrawer()} >
                <Text style={{ fontSize:20 }}>Lets Go!</Text>
              </Button>
            </View>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

export default Home;
