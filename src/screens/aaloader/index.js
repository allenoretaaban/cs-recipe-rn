import React, { Component } from 'react';
import {
  ActivityIndicator, AsyncStorage, StatusBar, StyleSheet, ImageBackground
} from 'react-native';
import { Container, Button, H3, Text, View, Input, Item, Spinner } from "native-base";

import cons from "./../../constants/Variables";

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    try {
      const value = await AsyncStorage.getItem('csrEmpId');
      if (value !== null) {
        console.log('retrieve data');
        this.props.navigation.navigate('Recipe');
      } else {
        this.props.navigation.navigate('Login');
      }
    } catch (error) {
      this.props.navigation.navigate('Login');
    }
  };

  // Render any loading content that you like here
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={cons.BACKGROUND} style={cons.MAINBG_STYLE}>
          <View style={cons.MAINBG_OVERLAY}>
            <ActivityIndicator style={{ flex:1, height:null }}/>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

export default AuthLoadingScreen;
