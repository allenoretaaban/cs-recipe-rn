import React from 'react';
import { Root } from "native-base";
import { Button, Image, View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import 'react-native-gesture-handler';

import SideBar from "./screens/sidebar";
import Home from "./screens/home/";
import Recipe from "./screens/aarecipe/";
import Create from "./screens/aarecipe/create";
import Update from "./screens/aarecipe/update";
import Login from "./screens/aalogin/";
import NHIcon from "./screens/icon/";
import AuthLoadingScreen from "./screens/aaloader/";

import Database from './db/Database';

const db = new Database();
console.clear();
db.initDB();

const Drawer = createDrawerNavigator(
  {
    Recipe: { screen: Recipe },
    Create: { screen: Create },
    Update: { screen: Update },
    Login: { screen: Login },
    Home: { screen: Home },
    NHIcon: { screen: NHIcon }
  },
  {
    initialRouteName: 'Recipe',
    contentOptions: {
      activeTintColor: '#e91e63'
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = createStackNavigator(
  {
    Drawer: { screen: Drawer },
    Login: { screen: Login },
    AuthLoadingScreen: { screen: AuthLoadingScreen }
  },
  {
    initialRouteName: 'AuthLoadingScreen',
    defaultNavigationOptions: {
      headerShown: false
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;

/*

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}*/
