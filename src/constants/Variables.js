import { Dimensions, Platform, StyleSheet } from "react-native";
import vars from "./../theme/variables/platform";

const launchscreenBg = require("../../assets/bg1.png");
const pf = Platform.OS;

export default {
  UNIT_OPTIONS: ["cup","drop","gram","gallon","kilo","ounce","piece","pint","pottle","pound","smidgen","tsp","Tbsp","quart"],
  // UNIT_OPTIONS: ["Select...","cup","drop","gram","gallon","kilo","ounce","piece","pint","pottle","pound","smidgen","tsp","Tbsp","quart"],
  UNIT_OPTIONS_C: [
    [1,1,"kg","Kilogram",0],
    [2,1,"g","Gram",0],
    [3,1,"mg","Milligram",0],
    [4,1,"oz","Ounce",0],
    [5,1,"lb","Pound",0],
    [6,2,"L","Liter",0],
    [7,2,"dl","Deciliter",0],
    [8,2,"cl","Centiliter",0],
    [9,2,"ml","Milliliter",0],
    [11,2,"fl oz","Fluid ounce",0],
    [12,2,"tbsp","Tablespoon",0],
    [13,2,"tsp","Teaspoon",0],
    [14,2,"cup","Cup",0],
    [15,2,"pt","Pint",0],
    [16,2,"qt","Quart",0],
    [17,2,"gal","Gallon",0],
    [18,3,"mm","Millimeters",0],
    [19,3,"cm","Centimeters",0],
    [20,3,"m","Meters",0],
    [21,4,"pc","Piece",0],
    [22,4,"gt","drop",0]
  ],
  DEVICE_HEIGHT: Dimensions.get("window").height,
  MODAL_HEIGHT: Dimensions.get("window").height-500,
  DEVICE_WIDTH: Dimensions.get("window").width,
  BACKGROUND: launchscreenBg,
  MAINBG_STYLE: { flex:1, width:null, height:null },
  MAINBG_OVERLAY: { flex:1, backgroundColor:'rgba(119,201,212, 0.3)' },
  TOAST_OPACITY: 0.8,
  TBL: { USER:"users", ITEM:"productitems", ING:"ingredients", STEP:"steps", RECIPE:"recipe" },

  STYLE_MTRIG: { width:50, height:46, alignItems:'center', justifyContent:'center' },
  STYLE_MOPT: { flex:1, height:46, borderBottomWidth:1, borderColor:vars.brandSuccess, flexDirection:'row', alignItems:'center' },
  STYLE_MOPT_I: { marginLeft:5, height:null, textAlignVertical:'center', color:'orange',  },
  STYLE_MOPT_T: { marginLeft:7, flext:1, height:null, textAlignVertical:'center', fontSize:pf==="ios"?16:14 },

  PRODITEM_EMPTY: {"dept":"", "id":0, "itemNo":"empty", "itemname":"No item/s found...", "recid":"", "selling_price":"", "unit":""},
  PRODITEM_SEARCHING: {"dept":"", "id":0, "itemNo":"searching", "itemname":"Searching...", "recid":"", "selling_price":"", "unit":""},

  PICKERSTYLES: StyleSheet.create({
    inputIOS: {
      height: null,
      fontSize: 18,
      paddingVertical: 12,
      paddingHorizontal: 10,
      color: 'gray',
      paddingRight: 10, // to ensure the text is never behind the icon
    },
    inputAndroid: {
      fontSize: 16,
      paddingLeft: 10,
      paddingVertical: 8,
      color: 'gray',
      marginRight: 1, // to ensure the text is never behind the icon
    },
  }),
  PICKERICON: { backgroundColor: 'transparent', borderTopWidth: 10, borderTopColor: 'gray', borderRightWidth: 10,
    borderRightColor: 'transparent', borderLeftWidth: 10, borderLeftColor: 'transparent', width: 0, height: 0,
  },

  LOREM: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

};
