import React, { Component } from "react";
import { Text, View } from "native-base";
import Toast, { DURATION } from "react-native-easy-toast";
import vars from "./../theme/variables/platform";

class Toastx extends Component {
  render() {
    return (
      /*<View style={{ alignItems:'center' }}>
        <Text>Hello!</Text>
      </View>*/
      <View>
        <Toast ref="eToast" style={{backgroundColor:vars.brandDanger,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={0.9} position='center' fadeOutDuration={1000}/>
        <Toast ref="iToast" style={{backgroundColor:vars.brandInfo,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={0.9} position='center' fadeOutDuration={1000}/>
        <Toast ref="sToast" style={{backgroundColor:vars.brandPrimary,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={0.9} position='center' fadeOutDuration={1000}/>
        <Toast ref="wToast" style={{backgroundColor:vars.brandWarning,paddingHorizontal:20,paddingVertical:15}} textStyle={{color:'white',fontSize:18}} opacity={0.9} position='center' fadeOutDuration={1000}/>
      </View>
    );
  }
}

export default Toastx;
